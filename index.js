//Khai báo thư viện express:
const express = require("express");

//Khai báo app
const app = express();

//Khai báo port
const port = 8000;

const methodMiddleWare = (req, res, next) => {
    console.log(req.method);
    next()
};
app.use((req, res, next) => {
    console.log(new Date());
    next();
}, methodMiddleWare
)

app.post('/', (req, res, next) => {
    console.log(req.method);
    next()
})

app.post('/', (req, res) => {
    console.log(`Post method`)

    res.status(200).json({
        
        message: `Post method`,
       
    })
})




app.get('/', (req, res) => {
    let today = new Date();
    console.log(`To day is ${today.getDate()} ${today.getMonth()} ${today.getFullYear()}`),

    res.status(200).json({
        
        message: `To day is ${today.getDate()} ${today.getMonth()} ${today.getFullYear()}`,
       
    })
})



//Khởi động app:
app.listen(port, () =>{
    console.log(`App listening on port ${port}`);
})

